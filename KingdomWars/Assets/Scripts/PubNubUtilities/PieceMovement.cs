﻿public class PieceMovement
{
    public int OldX { get; private set; }
    public int OldY { get; private set; }
    public int NewX { get; private set; }
    public int NewY { get; private set; }

    public PieceMovement(int oldX, int oldY, int newX, int newY)
    {
        OldX = oldX;
        OldY = oldY;
        NewX = newX;
        NewY = newY;
    }
}