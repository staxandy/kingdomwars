﻿public enum RoomCreationResult
{
    Success,
    RoomAlreadyExists,
    UnexpectedFailure
}