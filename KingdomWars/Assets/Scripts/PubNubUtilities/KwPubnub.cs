﻿using System;
using PubNubMessaging.Core;
using System.Collections.Generic;
using UnityEngine;

public class KwPubnub
{
    private const int maxPlayers = 2;
    private Pubnub pubnubClient;

    public event Action<RoomCreationResult> RoomCreated;
    public event Action<RoomJoinResult> RoomJoined;
    public event Action<PieceMovement> PieceMoved;

    public KwPubnub()
    {
        pubnubClient = new Pubnub(PubnubKey.PublishKey, PubnubKey.SubscribeKey);
    }

    public void CreateRoom(string name)
    {
        pubnubClient.HereNow(name,
            OnRoomCreateSuccessCallback,
            (x) => { NotifyRoomCreationResponse(RoomCreationResult.UnexpectedFailure); });
    }

    public void JoinRoom(string name)
    {
        pubnubClient.HereNow(name,
            OnRoomJoinSuccessCallback,
            (x) => { NotifyRoomJoinResult(RoomJoinResult.UnexpectedFailure); });
    }

    /// <summary>
    /// Sends the action of moving a piece on the board via Pubnub.
    /// </summary>
    /// <param name="movement"></param>
    /// <exception cref="System.InvalidOperationException">If no CreateRoom or JoinRoom was called beforehand.</exception>
    public void SendPieceMovement(string currentRoom, PieceMovement movement)
    {
        string serializedContent = pubnubClient.JsonPluggableLibrary.SerializeToJsonString(movement);

        pubnubClient.Publish(currentRoom,
            serializedContent,
            PublishPieceMovement,
            (x) => { });
    }

    // TODO : similar logic with OnRoomJoinSuccess
    private void OnRoomCreateSuccessCallback(object rawResponse)
    {
        try
        {
            List<object> response = rawResponse as List<object>;

            string requestedChannelName = response[1] as string;
            Dictionary<string, object> details = response[0] as Dictionary<string, object>;

            if (details["status"].ToString() != "200")
            {
                NotifyRoomCreationResponse(RoomCreationResult.UnexpectedFailure);
                return;
            }

            var attendees = Int64.Parse(details["occupancy"].ToString());
            if (attendees >= 1)
            {
                NotifyRoomCreationResponse(RoomCreationResult.RoomAlreadyExists);
                return;
            }

            pubnubClient.Subscribe(requestedChannelName,
                OnMessageReceivedCallback,
                (x) =>
                {
                    NotifyRoomCreationResponse(RoomCreationResult.Success);
                },
                (x) => { NotifyRoomCreationResponse(RoomCreationResult.UnexpectedFailure); });

        }
        catch (Exception e)
        {
            NotifyRoomCreationResponse(RoomCreationResult.UnexpectedFailure);
        }
    }

    private void OnMessageReceivedCallback(object rawResponse)
    {
        List<object> response = rawResponse as List<object>;
        string receivedContent = response[0].ToString();

        try
        {
            var pieceMovement = JsonUtility.FromJson<PieceMovement>(receivedContent);
            NotifyPieceMovement(pieceMovement);
        }
        catch
        {
            //Possible we have anything else except piece movement to handle?
        }
    }

    private void OnRoomJoinSuccessCallback(object rawResponse)
    {
        List<object> response = rawResponse as List<object>;

        string requestedChannelName = response[1] as string;
        Dictionary<string, object> details = response[0] as Dictionary<string, object>;

        if (details["status"].ToString() != "200")
        {
            NotifyRoomJoinResult(RoomJoinResult.UnexpectedFailure);
            return;
        }

        var attendees = Int64.Parse(details["occupancy"].ToString());
        if (attendees >= maxPlayers)
        {
            NotifyRoomJoinResult(RoomJoinResult.RoomFull);
            return;
        }

        pubnubClient.Subscribe(requestedChannelName,
            OnMessageReceivedCallback,
            (x) => { NotifyRoomJoinResult(RoomJoinResult.Success); },
            (x) => { NotifyRoomJoinResult(RoomJoinResult.UnexpectedFailure); });
    }

    private void PublishPieceMovement(object response)
    {

    }

    private void NotifyRoomCreationResponse(RoomCreationResult response)
    {
        var handler = RoomCreated;
        if (handler != null)
        {
            handler(response);
        }
    }

    private void NotifyRoomJoinResult(RoomJoinResult response)
    {
        var handler = RoomJoined;
        if (handler != null)
        {
            handler(response);
        }
    }

    private void NotifyPieceMovement(PieceMovement response)
    {
        var handler = PieceMoved;
        if (handler != null)
        {
            handler(response);
        }
    }
}
