﻿public enum MessageResult
{
    Success,
    UnknownRoom,
    UnexpectedFailure
}
