﻿public enum RoomJoinResult
{
    Success,
    RoomFull,
    UnexpectedFailure
}