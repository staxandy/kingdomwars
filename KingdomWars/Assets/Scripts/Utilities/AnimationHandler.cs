﻿using UnityEngine;
using System.Collections;
using KingdomWars.ChessModel;

namespace KingdomWars.Utilities
{
    public class AnimationHandler
    {
        private const float step = 0.2f;
        private const int liftingHeight = 2;

        public AnimationHandler()
        {
            currentState = AnimationState.FinishEnded;
            currentFinishState = FinishStates.FirstStep;
        }

        private AnimationState currentState;
        private FinishStates currentFinishState;

        public enum AnimationState
        {
            FinishEnded,
            StartStarted,
            StartEnded,
            FinishStarted
        }

        private enum FinishStates
        {
            FirstStep,
            SecondStep
        }

        public AnimationState CurrentState()
        {
            return currentState;
        }

        // Use this for initialization
        public void Start(GameObject piece/*, GameObject gameBoard*/)
        {
            if (currentState == AnimationState.FinishEnded || currentState == AnimationState.StartStarted)
            {
                var targetPosition = new Vector3(piece.transform.position.x, liftingHeight, piece.transform.position.z);
                GameController.PieceSourcePosition = new Position { Row = (int)piece.transform.position.z, Column = (int)piece.transform.position.x };

                if (piece.transform.position == targetPosition)
                {
                    currentState = AnimationState.StartEnded;
                    return;
                }

                piece.transform.position = Vector3.Lerp(piece.transform.position, targetPosition, step);
                currentState = AnimationState.StartStarted;
                Debug.Log("am intrat o data");
            }
        }

        public void Finish(GameObject piece, GameObject targetLocation)
        {
            if (currentState == AnimationState.StartEnded || currentState == AnimationState.FinishStarted)
            {
                if (currentFinishState == FinishStates.FirstStep)
                {
                    var targetPosition = new Vector3(targetLocation.transform.position.x, liftingHeight, targetLocation.transform.position.z);
                    if (piece.transform.position == targetPosition)
                    {
                        currentFinishState = FinishStates.SecondStep;
                        return;
                    }
                    piece.transform.position = Vector3.Lerp(piece.transform.position, targetPosition, step);
                }
                if (currentFinishState == FinishStates.SecondStep)
                {
                    var targetPosition = new Vector3(targetLocation.transform.position.x, targetLocation.transform.position.y + 0.5F, targetLocation.transform.position.z);
                    if (piece.transform.position == targetPosition)
                    {
                        currentFinishState = FinishStates.FirstStep;
                        currentState = AnimationState.FinishEnded;
                        return;
                    }
                    piece.transform.position = Vector3.Lerp(piece.transform.position, targetPosition, step);
                }

                currentState = AnimationState.FinishStarted;
            }
        }


        /// <summary>
        /// Moves the camera to the center of the game board 
        /// </summary>
        private void MoveCameraCenter()
        {
            //  gameCamera.transform.position = new Vector3(theGameBoard.transform.position.x, gameCamera.transform.position.y, theGameBoard.transform.position.z);
            //  gameCamera.transform.Rotate(90, 0, 0);//tilt the camera down by 90 degrees
        }

        /// <summary>
        /// moves the piece to the given x, y board coordinate e.g ( 1, f)
        /// </summary>
        /// <param name="piece">The game piece that will be moved</param>
        /// <param name="pozition">The position in game coordinates that the piece will be moved to</param>
        private void MovePiece(Vector3 position)
        {
            // thePiece.transform.position = Vector3.Lerp(thePiece.transform.position, new Vector3(position.x, liftingHeight, position.z), 0.5f);
            // thePiece.transform.position = Vector3.Lerp(thePiece.transform.position, new Vector3(position.x, 0.5f, position.z), 0.5f);
        }

    }
}