﻿using UnityEngine;
using System.Collections;
using KingdomWars;
using System.Linq;
using KingdomWars.ChessModel;

namespace KingdomWars.Utilities
{
    public class ChessPieceClickHandler : MonoBehaviour
    {
        private string boardPositionTag = "boardPosition";
        
        void OnMouseDown()
        {
            if (this.gameObject.tag == KingdomWars.GameController.PlayerColor)
            {
                if (KingdomWars.GameController.SelectedPiece != null && KingdomWars.GameController.SelectedPiece == this.gameObject)
                {
                    GameObject initialLocation = GameObject.FindGameObjectsWithTag("boardPosition").ToList().FirstOrDefault(o => o.transform.position.x == KingdomWars.GameController.SelectedPiece.transform.position.x && o.transform.position.z == KingdomWars.GameController.SelectedPiece.transform.position.z);
                    KingdomWars.GameController.SetNormalView();
                    KingdomWars.GameController.SelectedBoardPosition = initialLocation;
                }

                if (KingdomWars.GameController.SelectedPiece != null) return;

                KingdomWars.GameController.SetTopDownView();
                KingdomWars.GameController.SelectedPiece = this.gameObject;
                KingdomWars.GameController.SelectedPieceInitialPosition = new Vector2(this.gameObject.transform.position.x, this.gameObject.transform.position.z);
            }
            else if (this.gameObject.tag == boardPositionTag)
            {
                if (KingdomWars.GameController.SelectedPiece == null) return;

                GameController.PieceDestPosition = new Position { Row = (int)gameObject.transform.position.z, Column = (int)gameObject.transform.position.x };
                if (GameController.IsValidMove())
                {
                    KingdomWars.GameController.SetNormalView();
                    KingdomWars.GameController.SelectedBoardPosition = this.gameObject;
                }
            }
        }

        void OnMouseOver()
        {
            if (this.gameObject.tag == boardPositionTag && KingdomWars.GameController.SelectedPiece != null && KingdomWars.GameController.SelectedBoardPosition == null)
            {
                GetComponent<MeshRenderer>().enabled = true;
                return;
            }

            if (KingdomWars.GameController.PlayerColor != this.gameObject.tag && this.gameObject.tag != boardPositionTag)
            {
                this.gameObject.GetComponent<MeshCollider>().enabled = false;
                return;
            }

            Renderer renderer = GetComponent<Renderer>();
            renderer.material.SetColor("_SpecColor", Color.white);
        }

        void OnMouseExit()
        {
            if (this.gameObject.tag == boardPositionTag)
            {
                GetComponent<MeshRenderer>().enabled = false;
                return;
            }

            if (KingdomWars.GameController.PlayerColor != this.gameObject.tag) return;
            
            Renderer renderer = GetComponent<Renderer>();
            renderer.material.SetColor("_SpecColor", new Color(0.221F, 0.221F, 0.221F, 1));
        }

        
    }
}