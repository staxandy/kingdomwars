﻿namespace KingdomWars.ChessModel
{
    public enum PieceType
    {
        King,
        Queen,
        Rook,
        Bishop,
        Knight,
        Pawn,
        None
    }
}
