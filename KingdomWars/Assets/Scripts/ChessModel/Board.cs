﻿
using System;

namespace KingdomWars.ChessModel
{
    public class Board
    {
        public Square[,] Squares;

        public bool WhiteCheck;
        public bool WhiteMate;
        public bool BlackCheck;
        public bool BlackMate;
        public bool StaleMate;

        int FiftyMove;

        public Board()
        {
            Squares = new Square[8, 8];
            PopulateBoardWithPieces();
        }

        private void PopulateBoardWithPieces()
        {
            new FENBoardProvider().PopulateBoard(this);
        }

        public bool IsValidMove(Position sourcePosition, Position destinationPosition)
        {
            Piece piece = Squares[sourcePosition.Row, sourcePosition.Column].Piece;
            Piece destPiece = Squares[destinationPosition.Row, destinationPosition.Column].Piece;

            bool isValid = false;

            switch (piece.Type)
            {
                case PieceType.Pawn:
                    isValid = isValidPawnMove(sourcePosition, destinationPosition, piece, destPiece);
                    break;
                case PieceType.Knight:
                    isValid = isValidKnightMove(sourcePosition, destinationPosition, piece, destPiece);
                    break;
                case PieceType.Bishop:
                    isValid = isValidBishopMove(sourcePosition, destinationPosition, piece, destPiece);
                    break;
                case PieceType.Rook:
                    isValid = isValidRookMove(sourcePosition, destinationPosition, piece, destPiece);
                    break;
                case PieceType.Queen:
                    isValid = isValidRookMove(sourcePosition, destinationPosition, piece, destPiece) || isValidBishopMove(sourcePosition, destinationPosition, piece, destPiece); ;
                    break;
                case PieceType.King:
                    isValid = isValidKingMove(sourcePosition, destinationPosition, piece, destPiece);
                    break;
            }

            return isValid;
        }

        public void MovePiece(Position sourcePosition, Position destinationPosition)
        {
            //the destination position is equal to the source position => the piece did not move
            if(destinationPosition.Row == sourcePosition.Row && destinationPosition.Column == sourcePosition.Column)
            {
                return;
            }

            KingCastle(Squares[sourcePosition.Row, sourcePosition.Column].Piece, sourcePosition, destinationPosition);
            PromotePawn(Squares[sourcePosition.Row, sourcePosition.Column].Piece, destinationPosition, PieceType.Queen);

            Squares[destinationPosition.Row, destinationPosition.Column].Piece = Squares[sourcePosition.Row, sourcePosition.Column].Piece;
            Squares[sourcePosition.Row, sourcePosition.Column].Piece = new Piece { Type = PieceType.None };

            FiftyMove++;
            if (FiftyMove >= 50)
            {
                StaleMate = true;
            }
        }

        private bool isValidPawnMove(Position sourcePosition, Position destinationPosition, Piece piece, Piece destPiece)
        {
            bool isValidMove = false;

            int pawnStartRow = 1, direction = 1;
            if (piece.Color == PieceColor.Black)
            {
                direction = -1;
                pawnStartRow = 6;
            }

            //capture move
            if (destPiece.Type != PieceType.None && destPiece.Color != piece.Color && destPiece.Type != PieceType.King)
            {
                if (sourcePosition.Row + direction == destinationPosition.Row && sourcePosition.Column + 1 == destinationPosition.Column ||
                    sourcePosition.Row + direction == destinationPosition.Row && sourcePosition.Column - 1 == destinationPosition.Column)
                {
                    isValidMove = true;
                }
            }
            else if (destPiece.Type == PieceType.None)
            {
                if (sourcePosition.Row + direction == destinationPosition.Row && sourcePosition.Column == destinationPosition.Column ||
                        sourcePosition.Row == pawnStartRow && sourcePosition.Row + 2 * direction == destinationPosition.Row && sourcePosition.Column == destinationPosition.Column)
                {
                    isValidMove = true;
                }
            }


            return isValidMove;
        }

        private bool isValidKnightMove(Position sourcePosition, Position destinationPosition, Piece piece, Piece destPiece)
        {
            bool isValidMove = false;

            if (destPiece.Type == PieceType.None || destPiece.Type != PieceType.None && destPiece.Color != piece.Color && destPiece.Type != PieceType.King)
            {
                if (sourcePosition.Row + 2 == destinationPosition.Row && sourcePosition.Column - 1 == destinationPosition.Column ||
                    sourcePosition.Row - 2 == destinationPosition.Row && sourcePosition.Column + 1 == destinationPosition.Column ||
                    sourcePosition.Row - 2 == destinationPosition.Row && sourcePosition.Column - 1 == destinationPosition.Column ||
                    sourcePosition.Row + 2 == destinationPosition.Row && sourcePosition.Column + 1 == destinationPosition.Column ||
                    sourcePosition.Row - 1 == destinationPosition.Row && sourcePosition.Column + 2 == destinationPosition.Column ||
                    sourcePosition.Row + 1 == destinationPosition.Row && sourcePosition.Column - 2 == destinationPosition.Column ||
                    sourcePosition.Row - 1 == destinationPosition.Row && sourcePosition.Column - 2 == destinationPosition.Column ||
                    sourcePosition.Row + 1 == destinationPosition.Row && sourcePosition.Column + 2 == destinationPosition.Column)
                {
                    isValidMove = true;
                }
            }

            return isValidMove;
        }

        private bool isValidBishopMove(Position sourcePosition, Position destinationPosition, Piece piece, Piece destPiece)
        {
            bool isValidMove = false;

            if (destPiece.Type == PieceType.None || destPiece.Type != PieceType.None && destPiece.Color != piece.Color && destPiece.Type != PieceType.King)
            {
                if (Math.Abs(sourcePosition.Column - destinationPosition.Column) == Math.Abs(sourcePosition.Row - destinationPosition.Row))
                {
                    isValidMove = true;
                    int directionColumn = Math.Sign(destinationPosition.Column - sourcePosition.Column);
                    int directionRow = Math.Sign(destinationPosition.Row - sourcePosition.Row);
                    int jumps = Math.Abs(sourcePosition.Column - destinationPosition.Column);

                    for (int i = 1; i < jumps; i++)
                    {
                        if (Squares[sourcePosition.Row + i * directionRow, sourcePosition.Column + i * directionColumn].Piece.Type != PieceType.None)
                        {
                            isValidMove = false;
                            continue;
                        }
                    }
                }
            }

            return isValidMove;
        }

        private bool isValidRookMove(Position sourcePosition, Position destinationPosition, Piece piece, Piece destPiece)
        {
            bool isValidMove = false;

            if (destPiece.Type == PieceType.None || destPiece.Type != PieceType.None && destPiece.Color != piece.Color && destPiece.Type != PieceType.King)
            {
                if ((destinationPosition.Column == sourcePosition.Column && destinationPosition.Row != sourcePosition.Row) ||
                    (destinationPosition.Row == sourcePosition.Row && destinationPosition.Column != sourcePosition.Column))
                {
                    isValidMove = true;

                    int jumps = 0;
                    if (destinationPosition.Column == sourcePosition.Column)
                    {
                        int direction = Math.Sign(destinationPosition.Row - sourcePosition.Row);
                        jumps = Math.Abs(sourcePosition.Row - destinationPosition.Row);
                        for (int i = 1; i < jumps; i++)
                        {
                            if (Squares[sourcePosition.Row + i * direction, sourcePosition.Column].Piece.Type != PieceType.None)
                            {
                                isValidMove = false;
                                continue;
                            }
                        }
                    }
                    else
                    {
                        int direction = Math.Sign(destinationPosition.Column - sourcePosition.Column);
                        jumps = Math.Abs(sourcePosition.Column - destinationPosition.Column);
                        for (int i = 1; i < jumps; i++)
                        {
                            if (Squares[sourcePosition.Row, sourcePosition.Column + i * direction].Piece.Type != PieceType.None)
                            {
                                isValidMove = false;
                                continue;
                            }
                        }
                    }
                }
            }

            return isValidMove;
        }

        private bool isValidKingMove(Position sourcePosition, Position destinationPosition, Piece piece, Piece destPiece)
        {
            bool isValidMove = false;
            if (destPiece.Type == PieceType.None || destPiece.Type != PieceType.None && destPiece.Color != piece.Color && destPiece.Type != PieceType.King)
            {
                if (sourcePosition.Row + 1 == destinationPosition.Row && sourcePosition.Column + 1 == destinationPosition.Column ||
                    sourcePosition.Row == destinationPosition.Row && sourcePosition.Column + 1 == destinationPosition.Column ||
                    sourcePosition.Row - 1 == destinationPosition.Row && sourcePosition.Column + 1 == destinationPosition.Column ||
                    sourcePosition.Row - 1 == destinationPosition.Row && sourcePosition.Column == destinationPosition.Column ||
                    sourcePosition.Row - 1 == destinationPosition.Row && sourcePosition.Column - 1 == destinationPosition.Column ||
                    sourcePosition.Row == destinationPosition.Row && sourcePosition.Column - 1 == destinationPosition.Column ||
                    sourcePosition.Row + 1 == destinationPosition.Row && sourcePosition.Column - 1 == destinationPosition.Column ||
                    sourcePosition.Row + 1 == destinationPosition.Row && sourcePosition.Column == destinationPosition.Column)
                {
                    isValidMove = true;
                }

                //checking for king castle
                if (piece.Color == PieceColor.White)
                {
                    if (sourcePosition.Row == 0 && sourcePosition.Column == 4)
                    {
                        if (destinationPosition.Row == 0 && destinationPosition.Column == 2 && Squares[0, 0].Piece.Type == PieceType.Rook &&
                            Squares[0, 1].Piece.Type == PieceType.None && Squares[0, 2].Piece.Type == PieceType.None && Squares[0, 3].Piece.Type == PieceType.None)
                        {
                            isValidMove = true;
                        }
                        else if (destinationPosition.Row == 0 && destinationPosition.Column == 6 && Squares[0, 7].Piece.Type == PieceType.Rook &&
                          Squares[0, 5].Piece.Type == PieceType.None && Squares[0, 6].Piece.Type == PieceType.None)
                        {
                            isValidMove = true;
                        }
                    }
                }
                else
                {
                    if (sourcePosition.Row == 7 && sourcePosition.Column == 4)
                    {
                        if (destinationPosition.Row == 7 && destinationPosition.Column == 2 && Squares[7, 0].Piece.Type == PieceType.Rook &&
                            Squares[7, 1].Piece.Type == PieceType.None && Squares[7, 2].Piece.Type == PieceType.None && Squares[7, 3].Piece.Type == PieceType.None)
                        {
                            isValidMove = true;
                        }
                        else if (destinationPosition.Row == 7 && destinationPosition.Column == 6 && Squares[7, 7].Piece.Type == PieceType.Rook &&
                          Squares[7, 5].Piece.Type == PieceType.None && Squares[7, 6].Piece.Type == PieceType.None)
                        {
                            isValidMove = true;
                        }
                    }
                }
            }

            return isValidMove;
        }

        private void PromotePawn(Piece piece, Position position, PieceType promoteToPiece)
        {
            if (piece.Type == PieceType.Pawn)
            {
                if (position.Row == 0 || position.Row == 7)
                {
                    piece.Type = promoteToPiece;
                }
            }
        }

        private void KingCastle(Piece piece, Position sourcePosition, Position destPosition)
        {
            // [TODO] check that there are no pieces between the king and the rook
            // [TODO] check that the king is not in check

            if (piece.Type != PieceType.King)
            {
                return;
            }

            //check if this is a castling move.
            if (piece.Color == PieceColor.White && sourcePosition.Row == 0 && sourcePosition.Column == 4)
            {
                //Castle Right
                if (destPosition.Row == 0 && destPosition.Column == 6)
                {
                    //Move the Rook
                    if (Squares[0, 7].Piece.Type == PieceType.Rook)
                    {
                        Squares[0, 5].Piece = Squares[0, 7].Piece;
                        Squares[0, 7].Piece = new Piece { Type = PieceType.None };
                    }
                }

                //Castle Left
                else if (destPosition.Row == 0 && destPosition.Column == 2)
                {
                    //Move the Rook
                    if (Squares[0, 0].Piece.Type == PieceType.Rook)
                    {
                        Squares[0, 3].Piece = Squares[0, 0].Piece;
                        Squares[0, 0].Piece = new Piece { Type = PieceType.None };
                    }
                }
            }
            else if (piece.Color == PieceColor.Black && sourcePosition.Row == 7 && sourcePosition.Column == 4)
            {
                //Castle Right
                if (destPosition.Row == 7 && destPosition.Column == 6)
                {
                    //Move the Rook
                    if (Squares[7, 7].Piece.Type == PieceType.Rook)
                    {
                        Squares[7, 5].Piece = Squares[7, 7].Piece;
                        Squares[7, 7].Piece = new Piece { Type = PieceType.None };
                    }
                }

                //Castle Left
                else if (destPosition.Row == 7 && destPosition.Column == 2)
                {
                    //Move the Rook
                    if (Squares[7, 0].Piece.Type == PieceType.Rook)
                    {
                        Squares[7, 3].Piece = Squares[7, 0].Piece;
                        Squares[7, 0].Piece = new Piece { Type = PieceType.None };
                    }
                }
            }
        }
    }
}
