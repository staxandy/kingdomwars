﻿namespace KingdomWars.ChessModel
{
    public class FENBoardProvider
    {
        private const string fenNotation = "rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR";

        public void PopulateBoard(Board board)
        {
            string[] notationRows = fenNotation.Split('/');

            int row = 0, column;
            PieceColor color = PieceColor.Black;

            foreach (string notationRow in notationRows)
            {
                column = 0;

                foreach (char character in notationRow)
                {
                    if (character.ToString().Equals(character.ToString().ToLower()))
                    {
                        color = PieceColor.White;
                    }
                    else
                    {
                        color = PieceColor.Black;
                    }

                    if (char.IsNumber(character))
                    {
                        for (int i = 0; i < int.Parse(character.ToString()); i++)
                        {
                            board.Squares[row, column++] = new Square
                            {
                                Piece = new Piece
                                {
                                    Type = PieceType.None
                                }
                            };
                        }
                    }
                    else
                    {
                        switch (character.ToString().ToLower())
                        {
                            case "r":
                                board.Squares[row, column++] = new Square
                                {
                                    Piece = new Piece
                                    {
                                        Type = PieceType.Rook,
                                        Color = color
                                    }
                                };
                                break;
                            case "n":
                                board.Squares[row, column++] = new Square
                                {
                                    Piece = new Piece
                                    {
                                        Type = PieceType.Knight,
                                        Color = color
                                    }
                                };
                                break;
                            case "b":
                                board.Squares[row, column++] = new Square
                                {
                                    Piece = new Piece
                                    {
                                        Type = PieceType.Bishop,
                                        Color = color
                                    }
                                };
                                break;
                            case "q":
                                board.Squares[row, column++] = new Square
                                {
                                    Piece = new Piece
                                    {
                                        Type = PieceType.Queen,
                                        Color = color
                                    }
                                };
                                break;
                            case "k":
                                board.Squares[row, column++] = new Square
                                {
                                    Piece = new Piece
                                    {
                                        Type = PieceType.King,
                                        Color = color
                                    }
                                };
                                break;
                            case "p":
                                board.Squares[row, column++] = new Square
                                {
                                    Piece = new Piece
                                    {
                                        Type = PieceType.Pawn,
                                        Color = color
                                    }
                                };
                                break;
                            default:
                                board.Squares[row, column++] = new Square
                                {
                                    Piece = new Piece
                                    {
                                        Type = PieceType.None
                                    }
                                };
                                break;
                        }
                    }
                }
                row++;
            }
        }

    }
}
