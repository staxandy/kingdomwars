﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class MainMenuController : MonoBehaviour {

    private KwPubnub pubNub;
    private string roomName;

    void Start () {
        pubNub = new KwPubnub();
        pubNub.RoomCreated += RoomCreated;
        pubNub.RoomJoined += RoomJoined;
        KingdomWars.GameController.pubNub = pubNub;
    }

    private void RoomCreated(RoomCreationResult result)
    {
        if (result == RoomCreationResult.Success)
        {
            KingdomWars.GameController.PlayerColor = "white";
            KingdomWars.GameController.OponentColor = "black";
            SceneManager.LoadScene("MatchScene");
        }
    }

    private void RoomJoined(RoomJoinResult result)
    {
        if (result == RoomJoinResult.Success)
        {
            KingdomWars.GameController.PlayerColor = "black";
            KingdomWars.GameController.OponentColor = "white";
            SceneManager.LoadScene("MatchScene");
        }
    }

    public void CreateButtonClick(InputField roomNameText)
    {
        roomName = roomNameText.text;
        KingdomWars.GameController.RoomName = roomName;
        pubNub.CreateRoom(roomName);
    }

    public void JoinButtonClick(InputField roomNameText)
    {
        roomName = roomNameText.text;
        KingdomWars.GameController.RoomName = roomName;
        pubNub.JoinRoom(roomName);
    }
}
