﻿using UnityEngine;
using System.Collections;
using KingdomWars.ChessModel;
using KingdomWars.Utilities;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Linq;

namespace KingdomWars
{
    public class GameController : MonoBehaviour
    {
        public GameObject board_position;
        public GameObject white_pawn;
        public GameObject white_rook;
        public GameObject white_bishop;
        public GameObject white_knight;
        public GameObject white_queen;
        public GameObject white_king;
        public GameObject black_pawn;
        public GameObject black_rook;
        public GameObject black_bishop;
        public GameObject black_knight;
        public GameObject black_queen;
        public GameObject black_king;

        public static KwPubnub pubNub;

        public static GameObject SelectedPiece;
        public static GameObject SelectedBoardPosition;
        public static Vector2 SelectedPieceInitialPosition;

        public static Position PieceSourcePosition;
        public static Position PieceDestPosition;

        public static string PlayerColor;
        public static string OponentColor;
        public static string RoomName;

        private AnimationHandler animation;

        private static Board internalBoard;

        void Start()
        {
            animation = new AnimationHandler();
            InitialSetup();
            PopulateChessBoardPositions();
        }

        private void InitialSetup()
        {
            PubNubSetup();
            CameraSetup();
            internalBoard = new Board();
            PopulateBoard();
        }

        private void PubNubSetup()
        {
            pubNub.PieceMoved += PieceMovedHandler;
        }

        private void PieceMovedHandler(PieceMovement pieceMovement)
        {
            GameObject oponentPiece = GameObject.FindGameObjectsWithTag(OponentColor).ToList().FirstOrDefault(o => o.transform.position.x == pieceMovement.OldX && o.transform.position.z == pieceMovement.OldY);
            if (oponentPiece != null)
            {
                SelectedPiece = oponentPiece;

                GameObject newLocation = GameObject.FindGameObjectsWithTag("boardPosition").ToList().FirstOrDefault(o => o.transform.position.x == pieceMovement.NewX && o.transform.position.z == pieceMovement.NewY);
                if (newLocation != null)
                {
                    SelectedBoardPosition = newLocation;
                }
            }
        }

        private void SendPieceMovement()
        {
            PieceMovement pieceMovement = new PieceMovement((int)SelectedPieceInitialPosition.x, (int)SelectedPieceInitialPosition.y, (int)SelectedBoardPosition.transform.position.x, (int)SelectedBoardPosition.transform.position.z);
            pubNub.SendPieceMovement(RoomName, pieceMovement);
        }


        private void CameraSetup()
        {
            if(PlayerColor == "white")
            {
                Camera.main.transform.position = new Vector3(3.5F, 6, -3);
                Camera.main.transform.rotation = Quaternion.Euler(45, 0, 0);
            }
            else if (PlayerColor == "black")
            {
                Camera.main.transform.position = new Vector3(3.5F, 6, 10);
                Camera.main.transform.rotation = Quaternion.Euler(45, -180, 0);
            }
        }

        void Update()
        {
            MovePiece();
        }

        private void MovePiece()
        {
            if (animation.CurrentState() != AnimationHandler.AnimationState.FinishEnded)
            {
                if (animation.CurrentState() == AnimationHandler.AnimationState.StartStarted)
                {
                    animation.Start(SelectedPiece);
                }
                if (animation.CurrentState() == AnimationHandler.AnimationState.StartEnded || animation.CurrentState() == AnimationHandler.AnimationState.FinishStarted)
                {
                    if (SelectedBoardPosition != null)
                    {
                        PieceDestPosition = new Position { Row = (int)SelectedBoardPosition.transform.position.z, Column =  (int)SelectedBoardPosition.transform.position.x};
                        animation.Finish(SelectedPiece, SelectedBoardPosition);
                        if (animation.CurrentState() == AnimationHandler.AnimationState.FinishEnded)
                        {
                            SendPieceMovement();
                            SelectedPiece = null;
                            SelectedBoardPosition = null;
                            internalBoard.MovePiece(PieceSourcePosition, PieceDestPosition);
                            PopulateBoard();
                            return;
                        }
                    }
                }
                return;
            }

            if (SelectedPiece != null)
            {
                animation.Start(SelectedPiece);
            }
        }

        private void PopulateBoard()
        {
            DestroyAllPieces();
            for (int row = 0; row < 8; row++)
            {
                for (int column = 0; column < 8; column++)
                {
                    var piece = internalBoard.Squares[row, column].Piece;
                    switch (piece.Type)
                    {
                        case PieceType.Rook:
                            if (piece.Color == PieceColor.White) { PlacePiece(white_rook, row, column); }
                            else { PlacePiece(black_rook, row, column); }
                            break;
                        case PieceType.Knight:
                            if (piece.Color == PieceColor.White) { PlacePiece(white_knight, row, column); }
                            else { PlacePiece(black_knight, row, column); }
                            break;
                        case PieceType.Bishop:
                            if (piece.Color == PieceColor.White) { PlacePiece(white_bishop, row, column); }
                            else { PlacePiece(black_bishop, row, column); }
                            break;
                        case PieceType.Queen:
                            if (piece.Color == PieceColor.White) { PlacePiece(white_queen, row, column); }
                            else { PlacePiece(black_queen, row, column); }
                            break;
                        case PieceType.King:
                            if (piece.Color == PieceColor.White) { PlacePiece(white_king, row, column); }
                            else { PlacePiece(black_king, row, column); }
                            break;
                        case PieceType.Pawn:
                            if (piece.Color == PieceColor.White) { PlacePiece(white_pawn, row, column); }
                            else { PlacePiece(black_pawn, row, column); }
                            break;
                    }
                }
            }
        }

        private void PlacePiece(GameObject piece, int z, int x)
        {
            GameObject chessPiece = Instantiate(piece);
            chessPiece.transform.position = new Vector3(x, 0.75F, z);

            float chessScaleX = 0.75F * chessPiece.transform.localScale.x;
            float chessScaleY = 0.75F * chessPiece.transform.localScale.y;
            float chessScaleZ = 0.75F * chessPiece.transform.localScale.z;
            chessPiece.transform.localScale = new Vector3(chessScaleX, chessScaleY, chessScaleZ);
        }

        private void DestroyAllPieces()
        {
            GameObject[] blackPieces = GameObject.FindGameObjectsWithTag("black");
            foreach (GameObject piece in blackPieces)
            {
                Destroy(piece);
            }

            GameObject[] whitePieces = GameObject.FindGameObjectsWithTag("white");
            foreach (GameObject piece in whitePieces)
            {
                Destroy(piece);
            }
        }

        private void PopulateChessBoardPositions()
        {
            for (int x = 0; x < 8; x++)
            {
                for (int z = 0; z < 8; z++)
                {
                    AddChessBoardPosition(x, z);
                }
            }
        }

        private void AddChessBoardPosition(int x, int z)
        {
            GameObject chess_board_position = Instantiate(board_position);
            chess_board_position.transform.position = new Vector3(x, 0.1F, 7 - z);
            chess_board_position.transform.localScale = new Vector3(1, 0.1F, 1);
        }

        public static void SetTopDownView()
        {
            Animation animation = Camera.main.GetComponent<Animation>();
            if (PlayerColor == "white")
            {
                animation["TopDownViewWhite"].speed = 1;
                animation["TopDownViewWhite"].time = 0;
                animation.Play("TopDownViewWhite");
            }
            else
            {
                animation["TopDownViewBlack"].speed = 1;
                animation["TopDownViewBlack"].time = 0;
                animation.Play("TopDownViewBlack");
            }
        }

        public static void SetNormalView()
        {
            Animation animation = Camera.main.GetComponent<Animation>();
            if (PlayerColor == "white")
            {
                animation["TopDownViewWhite"].speed = -1;
                animation["TopDownViewWhite"].time = animation["TopDownViewWhite"].length;
                animation.Play("TopDownViewWhite");
            }
            else
            {
                animation["TopDownViewBlack"].speed = -1;
                animation["TopDownViewBlack"].time = animation["TopDownViewBlack"].length;
                animation.Play("TopDownViewBlack");
            }
        }

        public static bool IsValidMove()
        {
            return internalBoard.IsValidMove(PieceSourcePosition, PieceDestPosition);
        }

    }
}
